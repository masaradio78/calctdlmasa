# -----------------------------------------------------------------------------
# calc.py
#
# Expressions arithmétiques sans variables
# -----------------------------------------------------------------------------
from genereTreeGraphviz2 import printTreeGraph
import sys

reserved = {
    'print': 'PRINT',
    'if': 'IF',
    'else': 'ELSE',
    'while': 'WHILE',
    'for': 'FOR'
}

tokens = [
             'NUMBER', 'MINUS',
             'PLUS', 'TIMES', 'DIVIDE',
             'LPAREN', 'RPAREN',
             'AND', 'OR',
             'SEMICOLON',
             'ASSIGN',
             'EQUAL', 'NOT_EQUAL',
             'GREATER', 'LOWER', 'GREATER_EQUAL', 'LOWER_EQUAL',
             'NAME',
             'LBRACKET', 'RBRACKET'
         ] + list(reserved.values())

# Tokens
t_PLUS = r'\+'
t_MINUS = r'-'
t_TIMES = r'\*'
t_DIVIDE = r'/'
t_LPAREN = r'\('
t_RPAREN = r'\)'
t_AND = r'\&'
t_OR = r'\|'
t_SEMICOLON = r'\;'
t_ASSIGN = r'\='
t_EQUAL = r'\=\='
t_NOT_EQUAL = r'\!\='
t_GREATER = r'\>'
t_LOWER = r'\<'
t_GREATER_EQUAL = r'\>\='
t_LOWER_EQUAL = r'\<\='
t_LBRACKET = r'\{'
t_RBRACKET = r'\}'

def t_NUMBER(t):
    r'\d+'
    t.value = int(t.value)
    return t


def t_NAME(t):
    r'[a-zA-Z_][a-zA-Z_0-9]*'
    t.type = reserved.get(t.value, 'NAME')
    return t


# Ignored characters
t_ignore = " \t"


def t_newline(t):
    r'\n+'
    t.lexer.lineno += t.value.count("\n")


def t_error(t):
    print("Illegal character '%s'" % t.value[0])
    t.lexer.skip(1)


variables = {}

# Build the lexer
import ply.lex as lex

lex.lex()

precedence = (
    ('right', 'AND', 'OR'),
    ('nonassoc', 'LOWER', 'GREATER', 'LOWER_EQUAL', 'GREATER_EQUAL', 'EQUAL', 'NOT_EQUAL'),
    ('left', 'PLUS', 'MINUS'),
    ('left', 'TIMES', 'DIVIDE'),
    ('right', 'UMINUS')
)


def p_start(t):
    ''' start : linst'''
    print('tree=', t[1])
    printTreeGraph(t[1])
    evalInst(t[1])


def p_line(t):
    '''linst : linst statement
        | statement'''
    if len(t) == 2:
        t[0] = ('bloc', t[1], 'empty')
    if len(t) == 3:
        t[0] = ('bloc', t[1], t[2])


def p_statement_expr(p):
    'statement : expression SEMICOLON'

def p_expression_binop_plus(p):
    'expression : expression PLUS expression'
    p[0] = ('+', p[1], p[3])


def p_expression_binop_times(p):
    'expression : expression TIMES expression'
    p[0] = ('*', p[1], p[3])


def p_expression_binop_divide(p):
    'expression : expression DIVIDE expression'
    p[0] = ('/', p[1], p[3])


def p_expression_binop_minus(p):
    'expression : expression MINUS expression'
    p[0] = ('-', p[1], p[3])


def p_expression_binop_bool(p):
    '''expression : expression AND expression
    | expression OR expression'''
    if p[2] == '&':
        p[0] = ('&', p[1], p[3])
    if p[2] == '|':
        p[0] = ('|', p[1], p[3])


def p_expression_binop_bool_equal(p):
    'expression : expression EQUAL expression'
    p[0] = ('==', p[1], p[3])


def p_expression_binop_bool_not_equal(p):
    'expression : expression NOT_EQUAL expression'
    p[0] = ('!=', p[1], p[3])


def p_expression_binop_bool_greater(p):
    'expression : expression GREATER expression'
    p[0] = ('>', p[1], p[3])


# def p_expression_boolean_associatifs(p):
#     '''expression : expression LOWER expression LOWER expression'''
#     p[0] = p[1] < p[3] and p[3] < p[5]


def p_expression_binop_bool_lower(p):
    '''expression : expression LOWER expression'''
    p[0] = ('<', p[1], p[3])


def p_expression_binop_bool_greater_equal(p):
    'expression : expression GREATER_EQUAL expression'
    p[0] = ('>=', p[1], p[3])


def p_expression_binop_bool_lower_equal(p):
    'expression : expression LOWER_EQUAL expression'
    p[0] = ('<=', p[1], p[3])


def p_expression_group(p):
    'expression : LPAREN expression RPAREN'
    p[0] = p[2]


def p_expression_number(p):
    'expression : NUMBER'
    p[0] = p[1]


def p_expression_uminus(t):
    'expression : MINUS expression %prec UMINUS'
    t[0] = -t[2]


def p_expression_name(p):
    'expression : NAME'
    # p[0] = variables[p[1]]
    p[0] = p[1]


def p_statement_assign(p):
    'statement : NAME ASSIGN expression SEMICOLON'
    p[0] = ('assign', p[1], p[3])

# def p_expression_increment(p):
#     'expression : NAME PLUS PLUS'
#     p[0] = ('assign', p[1], ('+', p[1], 1))

def p_statement_print(p):
    'statement : PRINT LPAREN expression RPAREN SEMICOLON'
    p[0] = ('print', p[3])


def p_statement_condition_else(p):
    'statement : IF LPAREN expression RPAREN LBRACKET linst RBRACKET ELSE LBRACKET linst RBRACKET'
    p[0] = ('if', p[3], p[6], p[10])


def p_statement_condition(p):
    'statement : IF LPAREN expression RPAREN LBRACKET linst RBRACKET'
    p[0] = ('if', p[3], p[6])

def p_statement_while(p):
    'statement : WHILE LPAREN expression RPAREN LBRACKET linst RBRACKET'
    p[0] = ('while', p[3], p[6])

def p_statement_for(p):
    'statement : FOR LPAREN statement expression SEMICOLON statement RPAREN LBRACKET linst RBRACKET'
    p[0] = ('for', p[3], p[4], p[6], p[9])

def p_error(p):
    print("Syntax error at '%s'" % p.value)


import ply.yacc as yacc

yacc.yacc()


def evalInst(t):
    print('evalInst de ', t)
    if type(t) is not tuple:
        print('AST is not a tuple.')
        return
    if t[0] == 'bloc':
        evalInst(t[1])
        if t[2] != 'empty':
            evalInst(t[2])
    if t[0] == 'assign':
        variables[t[1]] = evalExpr(t[2])
    if t[0] == 'print':
        print('CALC>', evalExpr(t[1]))
    if t[0] == 'if':
        if evalExpr(t[1]):
            evalInst(t[2])
        elif len(t) > 3:
            evalInst(t[3])
    if t[0] == 'while':
        while evalExpr(t[1]):
            evalInst(t[2])
    if t[0] == 'for':
        evalInst(t[1])
        while evalExpr(t[2]):
            evalInst(t[4])
            evalInst(t[3])
    return None


def evalExpr(t):
    print('evalExpr de ', t)
    if type(t) is tuple:
        if t[0] == '+': return evalExpr(t[1]) + evalExpr(t[2])
        if t[0] == '-': return evalExpr(t[1]) - evalExpr(t[2])
        if t[0] == '/': return evalExpr(t[1]) / evalExpr(t[2])
        if t[0] == '*': return evalExpr(t[1]) * evalExpr(t[2])

        if t[0] == '==': return evalExpr(t[1]) == evalExpr(t[2])
        if t[0] == '!=': return evalExpr(t[1]) != evalExpr(t[2])
        if t[0] == '<': return evalExpr(t[1]) < evalExpr(t[2])
        if t[0] == '>': return evalExpr(t[1]) > evalExpr(t[2])
        if t[0] == '<=': return evalExpr(t[1]) <= evalExpr(t[2])
        if t[0] == '>=': return evalExpr(t[1]) >= evalExpr(t[2])
        if t[0] == '&': return evalExpr(t[1]) & evalExpr(t[2])
        if t[0] == '|': return evalExpr(t[1]) | evalExpr(t[2])
    if type(t) is int:
        return t
    if t in variables:
        return variables[t]
    return 'UNK'

if len(sys.argv) >= 2:
    path = sys.argv[1]
    f = open(path, 'r')
    s = f.read()

    yacc.parse(s)
else:
    s = input('> ')
    yacc.parse(s)
    yacc.parse(s)
