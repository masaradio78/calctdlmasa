# -----------------------------------------------------------------------------
# calc.py
#
# Expressions arithmétiques sans variables
# -----------------------------------------------------------------------------

import sys

import ply.yacc as yacc
import ply.lex as lex

import uuid
import graphviz as gv


def printTreeGraph(t):
    graph = gv.Digraph(format='pdf')
    graph.attr('node', shape='circle')
    addNode(graph, t)
    # graph.render(filename='img/graph') #Pour Sauvegarder
    graph.view()  # Pour afficher


def addNode(graph, t):
    myId = uuid.uuid4()

    if type(t) != tuple:
        graph.node(str(myId), label=str(t))
        return myId

    graph.node(str(myId), label=str(t[0]))
    for i in range(1, len(t)):
        graph.edge(str(myId), str(addNode(graph, t[i])), arrowsize='0')

    return myId


reserved = {
    'if': 'IF',
    'while': 'WHILE',
    'for': 'FOR',
    'in': 'IN',
    'print': 'PRINT',
    'range': 'RANGE',
    'function': 'FUNCTION',
    'void': 'VOID',
    'return': 'RETURN'
}

tokens = [
    'COMMA',
    'SEMI_COLON', 'COMMA',
    'NAME',
    'NUMBER',
    'MINUS',
    'EQUAL',
    'PLUS', 'TIMES', 'DIVIDE',
    'BOOLEQ', 'BOOLNEQ', 'BOOLGT', 'BOOLGTE', 'BOOLLT', 'BOOLLTE',
    'LPAREN', 'RPAREN',
    'LBRACKET', 'RBRACKET',
] + list(reserved.values())

# Tokens
t_COMMA = r'\,'
t_SEMI_COLON = r'\;'
t_COMMA = r'\,'
t_PLUS = r'\+'
t_MINUS = r'\-'
t_TIMES = r'\*'
t_DIVIDE = r'\/'
t_EQUAL = r'\='
t_BOOLEQ = r'\=\='
t_BOOLNEQ = r'\!\='
t_BOOLGT = r'\>'
t_BOOLGTE = r'\>='
t_BOOLLT = r'\<'
t_BOOLLTE = r'\<='
t_LPAREN = r'\('
t_RPAREN = r'\)'
t_LBRACKET = r'\{'
t_RBRACKET = r'\}'


def t_NUMBER(t):
    r'\d+'
    t.value = int(t.value)
    return t


def t_NAME(t):
    r'[a-zA-Z_][a-z-A-Z0-9_]*'

    if t.value in reserved:
        t.type = reserved[t.value]

    return t


# Ignored characters
t_ignore = " \t"


def t_newline(t):
    r'\n+'
    t.lexer.lineno += t.value.count("\n")

# gérer les scopes via une stack

def t_error(t):
    print("Illegal character '%s'" % t.value[0])
    t.lexer.skip(1)


variables = {}

# Build the lexer
lex.lex()

precedence = (
    ('left', 'MINUS', 'PLUS'),
    ('left', 'TIMES', 'DIVIDE'),
)


def p_real_start(p):
    'begin : body'
    p[0] = p[1]

    printTreeGraph(p[0])
    eval(p[0])


def p_start(p):
    '''body : instruction body
        | instruction'''

    if len(p) == 3:
        p[0] = ('block', p[1], p[2])
    else:
        p[0] = p[1]


def p_body(p):
    '''instruction : statement
        | expression SEMI_COLON'''
    p[0] = p[1]


def p_block(p):
    'block : LBRACKET body RBRACKET'
    p[0] = ('block', p[2])

def p_block_statement(p):
    'statement : block'
    p[0] = p[1]

def p_if(p):
    'statement : IF expression statement'
    p[0] = ('If', p[2], p[3])


def p_function(p):
    'statement : FUNCTION NAME LPAREN args RPAREN block'
    p[0] = ('Function', False, p[2], p[4], p[6])

def p_void_function(p):
    'statement : VOID NAME LPAREN args RPAREN block'
    p[0] = ('Function', True, p[2], p[4], p[6])

def p_return (p):
    'statement : RETURN expression SEMI_COLON'
    p[0] = ('Return', p[2])


def p_empty_return(p):
    'statement : RETURN SEMI_COLON'
    p[0] = ('VoidReturn',)

def p_expression_call(p):
    'expression : NAME lPAREN call_args RPAREN'
    p[0] = ('Call', p[1], p[3])

def p_expression_call_empty(p):
    'expression : NAME lPAREN RPAREN'
    p[0] = ('Call', p[1], ('Args',))

def p_no_args(p):
    'call_args :'
    p[0] = ('Args', )

def p_args(p):
    'call_args : NAME'
    p[0] = ('Args', p[1])

def p_multiple_args(p):
    'call_args : NAME COMMA call_args'
    p[0] = ('Args', p[1], *p[3][1::])

def p_while(p):
    'statement : WHILE expression statement'
    p[0] = ('While', p[2], p[3])


def p_for(p):
    'statement : FOR NAME IN expression statement'
    p[0] = ('For', p[2], p[4], p[5])


def p_expression_binop_plus(p):
    'expression : expression PLUS expression'
    p[0] = ('+', p[1], p[3])


def p_expression_binop_minus(p):
    'expression : expression MINUS expression'
    p[0] = ('-', p[1], p[3])


def p_expression_binop_times(p):
    'expression : expression TIMES expression'
    p[0] = ('*', p[1], p[3])


def p_expression_binop_divide(p):
    'expression : expression DIVIDE expression'
    p[0] = ('/', p[1], p[3])


def p_expression_binop_booleq(p):
    'expression : expression BOOLEQ expression'
    p[0] = ('==', p[1], p[3])


def p_expression_binop_boolneq(p):
    'expression : expression BOOLNEQ expression'
    p[0] = ('!=', p[1], p[3])


def p_expression_binop_boolgt(p):
    'expression : expression BOOLGT expression'
    p[0] = ('>', p[1], p[3])


def p_expression_binop_boolgte(p):
    'expression : expression BOOLGTE expression'
    p[0] = ('>=', p[1], p[3])


def p_expression_binop_boollt(p):
    'expression : expression BOOLLT expression'
    p[0] = ('<', p[1], p[3])


def p_expression_binop_boollte(p):
    'expression : expression BOOLLTE expression'
    p[0] = ('<=', p[1], p[3])


def p_expression_equal(p):
    'expression : NAME EQUAL expression'
    p[0] = ('=', p[1], p[3])


def p_expression_group(p):
    'expression : LPAREN expression RPAREN'
    p[0] = p[2]


def p_expression_number(p):
    'expression : NUMBER'
    p[0] = ('Num', p[1])


def p_expression_variable(p):
    'expression : NAME'
    p[0] = ('Name', p[1])


def p_expression_print(p):
    'expression : PRINT LPAREN expression RPAREN'
    p[0] = ('Print', p[3])


def p_expression_range(p):
    'expression : RANGE LPAREN expression COMMA expression RPAREN'
    p[0] = ('Range', p[3], p[5])


def p_error(p):
    print("Syntax error at '%s'" % p.value)


yacc.yacc()

class Function:
    def __init__(self, is_void, scope, args, block):
        self.scope = scope
        self.args = args
        self.block = block
        self.is_void = is_void

class Scope:
    def __init__(self, parent = None):
        self.variables = {}
        self.functions = {}
        self.parent = parent

    def get_variable(self, name: str):
        if name in self.variables:
            return self.variables[name]
        if self.parent is not None:
            return self.parent.get_variable(name)

        return None

    def has_variable(self, name: str):
        return name in self.variables

    def set_variable(self, name, value):
        if self.has_variable(name):
            self.variables[name] = value
            return value
        if self.parent is not None:
            return self.parent.set_variable(name, value)

        return None

    def get_function(self, name):
        if name is self.functions:
            return name in self.functions
    def set_function(self, name, value):
        if self.get_function(name):
            self.functions[name] = value
            return value
        if self.parent is not None:
            return self.parent.set_variable(name, value)

        return None


scopes = [Scope()]

def eval(p):
    if type(p) is not tuple:
        print('AST is not a tuple.')
        return

    scope = scopes[-1]
    if p[0] == 'block':
        scopes.append(Scope(scope))

        for i in range(1, len(p)):
            event = eval(p[i])

            if type(event) is tuple and event[0] == 'Return':
                return_event = event
                break

        for i in range(1, len(p)):
            eval(p[i])
        scopes.pop()

        return return_event
    elif p[0] == 'If':
        if eval(p[1]):
            eval(p[2])
    elif p[0] == 'Function':
        function = Function(p[1], scope, p[3])
        scope.functions[p[2]] = function
        return function
    elif p[0] == 'Return':
        if len(p) == 2:
            return ('Return', eval(p[1]))
        return ('Return',)
    elif p[0] == 'VoidReturn':
        return ('Return',)
    elif p[0] == 'Call':
        function = scope.get_function(p[1])
        if function is None:
            print(f'Undefined function')
            exit(1)
        args = p[2][1::]
        scopes.append(function.scope)

        if len(args) != len(function.args):
            print(f'Function {p[1]} need {len(function.arg)} arguments, {len(args)} passed.', file=sys.stderr)

        for i in range(len(function.args)):
            function.scope.variables[function.args[i]] = eval(args[i])
        eval(function.block)

        event = eval(function.block)
        scopes.pop()

        if type(event) is tuple and event[0] == 'Return':
            if function.is_void and len(event) == 2:
                print('Can\'t return invoid function')
                exit(1)
            return event[1]

    elif p[0] == 'While':
        while eval(p[1]):
            eval(p[2])
    elif p[0] == 'For':
        for i in eval(p[2]):
            variables[p[1]] = i
            eval(p[3])
    elif p[0] == '+':
        return eval(p[1]) + eval(p[2])
    elif p[0] == '-':
        return eval(p[1]) - eval(p[2])
    elif p[0] == '*':
        return eval(p[1]) * eval(p[2])
    elif p[0] == '/':
        return eval(p[1]) / eval(p[2])
    elif p[0] == '=':
        value = eval(p[2])
        var = scope.set_variable(p[1], value)

        if var is None: # pour gérer le cas d'une variable globale
            scope.variables[p[1]] = value
            return value

        return var
    elif p[0] == '==':
        return eval(p[1]) == eval(p[2])
    elif p[0] == '!=':
        return eval(p[1]) != eval(p[2])
    elif p[0] == '>':
        return eval(p[1]) > eval(p[2])
    elif p[0] == '>=':
        return eval(p[1]) >= eval(p[2])
    elif p[0] == '<':
        return eval(p[1]) < eval(p[2])
    elif p[0] == '<=':
        return eval(p[1]) <= eval(p[2])
    elif p[0] == 'Num':
        return int(p[1])
    elif p[0] == 'Name':
        var = scope.get_variables[p[1]]
        if var is None:
            print(f'Undefined variable {p[1]}', file=sys.stderr)
            exit(1)
        return var
    elif p[0] == 'Print':
        print(eval(p[1]))
    elif p[0] == 'Range':
        return range(eval(p[1]), eval(p[2]))

    return None


if len(sys.argv) >= 2:
    path = sys.argv[1]
    f = open(path, 'r')
    s = f.read()

    yacc.parse(s)
else:
    s = input('> ')
    yacc.parse(s)
